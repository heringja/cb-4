%expect 0

%define api.parser.struct {Parser}
%define api.value.type {Value}
%define api.parser.check_debug { self.debug }

%define parse.error custom
%define parse.trace

%code use {
    // all use goes here
    use crate::{Token, C1Lexer as Lexer, Loc, Value};
}

%code parser_fields {
    errors: Vec<String>,
    /// Enables debug printing
    pub debug: bool,
}

%token
    AND           "&&"
    OR            "||"
    EQ            "=="
    NEQ           "!="
    LEQ           "<="
    GEQ           ">="
    LSS           "<"
    GRT           ">"
    LPAREN        "("
    RPAREN        ")"
    LBRACE        "{"
    RBRACE        "}"
	SEMI		  ";"
	COMMA		  ","
	PLUS		  "+"
	MINUS		  "-"
	MULT		  "*"
	DIVIDE		  "/"
	ASSIGN		  "="
    KW_BOOLEAN    "bool"
    KW_DO         "do"
    KW_ELSE       "else"
    KW_FLOAT      "float"
    KW_FOR        "for"
    KW_IF         "if"
    KW_INT        "int"
    KW_PRINTF     "printf"
    KW_RETURN     "return"
    KW_VOID       "void"
    KW_WHILE      "while"
    CONST_INT     "integer literal"
    CONST_FLOAT   "float literal"
    CONST_BOOLEAN "boolean literal"
    CONST_STRING  "string literal"
    ID            "identifier"

// definition of association and precedence of operators
%left PLUS MINUS OR
%left MULT DIVIDE AND
%nonassoc UMINUS

// workaround for handling dangling else
// LOWER_THAN_ELSE stands for a not existing else
%nonassoc LOWER_THAN_ELSE
%nonassoc KW_ELSE

%%
program             : YYEOF                                                          												{$$ = Value::None;}
                      |program declassignment                             							                                {$$ = Value::None;}
                      |program functiondefinition							                                                        {$$ = Value::None;}
                      |declassignment                             							                                		{$$ = Value::None;}
                      |functiondefinition							                                                        		{$$ = Value::None;}

functiondefinition  : type ID LPAREN parameterlist RPAREN LBRACE statementlist RBRACE                                               {$$ = Value::None;}
					  | type ID LPAREN RPAREN LBRACE statementlist RBRACE                                                      		{$$ = Value::None;}
parameterlist       : type ID                                                                                     					{$$ = Value::None;}
						| parameterlist COMMA type ID                                                                               {$$ = Value::None;}
functioncall        : ID LPAREN assignment mcommaassignment RPAREN                                        							{$$ = Value::None;}
					  | ID LPAREN assignment RPAREN                                        											{$$ = Value::None;}
					  | ID LPAREN  RPAREN                                        													{$$ = Value::None;}

mcommaassignment    : mcommaassignment COMMA assignment                                                                             {$$ = Value::None;}
                      | COMMA assignment                                                                                           	{$$ = Value::None;}

statementlist       : %empty																										{$$ = Value::None;}
					  | statementlistrec                                                                                    	 	{$$ = Value::None;}
statementlistrec    : statblock																										{$$ = Value::None;}
					  | statementlistrec statblock                                                                                  {$$ = Value::None;}

statement           : ifstatement                                                                                                   {$$ = Value::None;}
                      | forstatement														                                        {$$ = Value::None;}
                      | whilestatement														                                        {$$ = Value::None;}
                      | returnstatement SEMI 												                                        {$$ = Value::None;}
                      | dowhilestatement SEMI												                                        {$$ = Value::None;}
                      | printf SEMI															                                        {$$ = Value::None;}
                      | declassignment													                                        	{$$ = Value::None;}
                      | statassignment SEMI													                                        {$$ = Value::None;}
                      | functioncall SEMI													                                        {$$ = Value::None;}
statblock           : LBRACE statementlist RBRACE													                                {$$ = Value::None;}
                      | statement															                                        {$$ = Value::None;}

ifstatement         : KW_IF LPAREN assignment RPAREN statblock														    			%prec LOWER_THAN_ELSE
																																	{$$ = Value::None;}
					  | KW_IF LPAREN assignment RPAREN statblock KW_ELSE statblock													{$$ = Value::None;}
forstatement        : KW_FOR LPAREN statassignment SEMI expr SEMI statassignment RPAREN statblock									{$$ = Value::None;}
				    	| KW_FOR LPAREN declassignment expr SEMI statassignment RPAREN statblock									{$$ = Value::None;}
dowhilestatement    : KW_DO statblock KW_WHILE LPAREN assignment RPAREN																{$$ = Value::None;}
whilestatement      : KW_WHILE LPAREN assignment RPAREN statblock																	{$$ = Value::None;}
returnstatement     : KW_RETURN assignment	                                                                                        {$$ = Value::None;}
                        | KW_RETURN																				                    {$$ = Value::None;}
printf              : KW_PRINTF LPAREN assignment RPAREN																			{$$ = Value::None;}
						| KW_PRINTF LPAREN CONST_STRING RPAREN																		{$$ = Value::None;}
declassignment      : type ID "=" assignment SEMI                                                                                   {$$ = Value::None;}
                        | type ID SEMI    																					        {$$ = Value::None;}

statassignment      : ID "=" assignment																								{$$ = Value::None;}
assignment          : statassignment																								{$$ = Value::None;}
                        | expr																										{$$ = Value::None;}
expr                : simpexpr "==" simpexpr                                                                                        {$$ = Value::None;}
                        | simpexpr "!=" simpexpr                                                                                    {$$ = Value::None;}
                        | simpexpr "<=" simpexpr                                                                                    {$$ = Value::None;}
                        | simpexpr ">=" simpexpr                                                                                    {$$ = Value::None;}
                        | simpexpr "<" simpexpr                                                                                     {$$ = Value::None;}
                        | simpexpr ">" simpexpr                                                                                     {$$ = Value::None;}
                        | simpexpr		                                                                                            {$$ = Value::None;}
simpexpr            : "-" term mopterm													                                            {$$ = Value::None;}
                        | term mopterm																								{$$ = Value::None;}
mopterm             : mopterm opterm																								{$$ = Value::None;}
                        | %empty																									{$$ = Value::None;}
opterm              : "+" term																										{$$ = Value::None;}
                        | "-" term																									{$$ = Value::None;}
                        | "||" term																									{$$ = Value::None;}
term                : factor 																										{$$ = Value::None;}
                        | factor oterm																								{$$ = Value::None;}
oterm               : oterm mterm	                                                                                                {$$ = Value::None;}
                        | mterm														                                                {$$ = Value::None;}
mterm               : "*" factor                                                                                                    {$$ = Value::None;}
                        | "/" factor 																								{$$ = Value::None;}
                        | "&&" factor																								{$$ = Value::None;}

factor              : CONST_INT																										{$$ = Value::None;}
                      | CONST_FLOAT																									{$$ = Value::None;}
                      | CONST_BOOLEAN																								{$$ = Value::None;}
                      | functioncall																								{$$ = Value::None;}
                      | ID																											{$$ = Value::None;}
                      | LPAREN assignment RPAREN																					{$$ = Value::None;}

type                : KW_BOOLEAN																									{$$ = Value::None;}
                      | KW_FLOAT																									{$$ = Value::None;}
                      | KW_INT																										{$$ = Value::None;}
                      | KW_VOID																										{$$ = Value::None;}


%%

impl Parser {
    /// "Sucess" status-code of the parser
    pub const ACCEPTED: i32 = -1;

    /// "Failure" status-code of the parser
    pub const ABORTED: i32 = -2;

    /// Constructor
    pub fn new(lexer: Lexer) -> Self {
        // This statement was added to manually remove a dead code warning for 'owned_value_at' which is auto-generated code
        Self::remove_dead_code_warning();
        Self {
            yy_error_verbose: true,
            yynerrs: 0,
            debug: false,
            yyerrstatus_: 0,
            yylexer: lexer,
            errors: Vec::new(),
        }
    }

    /// Wrapper around generated `parse` method that also
    /// extracts the `errors` field and returns it.
    pub fn do_parse(mut self) -> Vec<String> {
        self.parse();
        self.errors
    }

    /// Retrieve the next token from the lexer
    fn next_token(&mut self) -> Token {
        self.yylexer.yylex()
    }

    /// Print a syntax error and add it to the errors field
    fn report_syntax_error(&mut self, stack: &YYStack, yytoken: &SymbolKind, loc: YYLoc) {
        let token_name = yytoken.name();
        let error = format!("Unexpected token {} at {:?}", token_name, loc);
        eprintln!("Stack: {}\nError: {}", stack, error);
        self.errors.push(error);
    }

    /// Helper function that removes a dead code warning, which would otherwise interfere with the correction of a submitted
    /// solution
    fn remove_dead_code_warning() {
    	let mut stack = YYStack::new();
    	let yystate: i32 = 0;
    	let yylval: YYValue = YYValue::new_uninitialized();
    	let yylloc: YYLoc = YYLoc { begin: 0, end: 0 };
        stack.push(yystate, yylval.clone(), yylloc);
    	let _ = stack.owned_value_at(0);
    }
}